import { SafeAreaView, StyleSheet, Text, View, StatusBar, Image, TextInput, TouchableOpacity, ImageBackground } from 'react-native'
import React, { useState } from 'react';
import { instance as axios } from '../util/api';

const Regis = () => {

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const registrasi = async () => {
    try {
      const { data, status } = await axios.post("/register", { username: username, password: password });
      console.log(status);
      if (status === 200) {
        navigation.navigate("Login");
      } else {
        alert("Register Fail")
      }
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <ImageBackground style={styles.backgroundi} source={require("../assets/christies-auction.jpg")} >
      <View style={styles.container}>


        <Image style={{ height: 50, width: 150, resizeMode: 'stretch', backgroundColor: 'rgba(0, 0, 0, 0.8)', borderRadius: 8 }} source={require("../assets/auction-today-shadow-white.png")} />
        <View style={styles.loginArea}>
          <TextInput style={styles.inputStyle} onChangeText={setUsername} value={username} placeholder="Username" />
          <TextInput secureTextEntry={true} style={styles.inputStyle} onChangeText={setPassword} value={password} placeholder="Password" />
        </View>
        <TouchableOpacity style={styles.button} onPress={registrasi}>
          <Text>Register</Text>
        </TouchableOpacity>

      </View >
    </ImageBackground>
  )
}

export default Regis

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#99d6ff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: StatusBar.currentHeight
  },
  loginArea: {
    margin: 10
  },
  inputStyle: {
    width: 200,
    borderRadius: 10,
    margin: 5,
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#99b3ff',
    color: 'white'
  },
  textStyle: {
    margin: 10,
    color: 'white'
  },
  button: {
    width: "40%",
    borderRadius: 30,
    padding: 5,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: "#88ff4d",
  },
  signup: {
    borderRadius: 30,
    padding: 1,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: 'rgba(255,255,255, 0.3)'
  },
  backgroundi: {
    flex: 1
  }
});