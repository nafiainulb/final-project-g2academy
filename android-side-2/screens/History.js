import { View, Text, StyleSheet, ScrollView, FlatList } from 'react-native'
import React from 'react'
import HistoryItem from '../components/HistoryItem';
import { useSelector } from 'react-redux';

const History = ({ navigation }) => {

  const mHistory = useSelector(state => state.mHistory);

  return (
    // <ScrollView>
      <View style={styles.container}>
      <FlatList
          data={mHistory}
          renderItem={({ item }) => <HistoryItem item={item} navigation={navigation} />}
        />
      </View>
    // </ScrollView>
  )
}

export default History

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
})