import { ActivityIndicator, Button, FlatList, Image, Modal, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { getFormatedDate } from 'react-native-modern-datepicker'
import { formatCurrency } from 'react-native-format-currency'
import { instance as axios } from '../util/api';
import { useSelector, useDispatch } from 'react-redux';

const DetailItem = ({ route }) => {

  const dTail = useSelector(state => state.dTail);
  const mBidder = useSelector(state => state.mBidder);
  const dispact = useDispatch();
  const message = useSelector(state => state.message);
  const mToken = useSelector(state => state.mToken);

  const [numBid, setNumBid] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [result, setResult] = useState('');

  const getItem = async (id) => {
    console.log("run get item");
    try {
      const { data, status } = await axios.get(`/api/product/${id}`);
      if (status === 200) {
        dispact({ type: "SET_DETAIL", payload: data })
      }
    } catch (err) {
      console.log(err);
    }
  }

  const getBidder = async (id) => {
    console.log("run get bidder");
    try {
      const { data, status } = await axios.get(`/api/bidder/${id}`);
      if (status === 200) {
        dispact({ type: "SET_BIDDER", payload: data })
      }
    } catch (err) {
      console.log(err);
    }
  }
  const bidding = async () => {
    console.log("run bidding item");
    setResult('');
    setModalVisible(!modalVisible);
    try {
      const { data, status } = await axios.post(`/api/bid/${route.params.id}`, { bid: numBid }, { headers: { Authorization: `Bearer ${mToken}` } });
      if (status === 200) {
        console.log(data);
        setResult(data);
        dispact({ type: "SET_MESSAGE", payload: { action: "BEGIN", payload: route.params.id } })
      }
    } catch (err) {
      setResult("Bid Fail")
    }
  }

  useEffect(() => {
    console.log("run effect detail");
    getItem(route.params.id);
    getBidder(route.params.id);
  }, [])

  useEffect(() => {
    console.log("Renew bidder");
    if (message.action === "BEGIN") {
      console.log("Renew bidder v1");
      { message.payload === route.params.id ? console.log("same") : console.log("not same");; }
      if (message.payload === route.params.id) {
        console.log("Renew bidder v2");
        getBidder(route.params.id);
        getItem(route.params.id);
      }
    }
  }, [message])

  const Popup = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            {
              result === '' ?
                <View>
                  <Text style={styles.modalText}>Loading</Text>
                  <ActivityIndicator />
                </View>
                :
                <View>
                  <Text style={styles.modalText}>{result}</Text>
                  <Button title='Close' onPress={() => setModalVisible(!modalVisible)} />
                </View>
            }
          </View>
        </View>
      </Modal>
    )
  }

  return (
    <ScrollView>
      <Popup />
      <View style={styles.box}>
        <View>
          <Image source={{ uri: dTail.image }} style={styles.image} />
          <View style={styles.teks}>
            <Text style={[styles.teksBold, { fontSize: 13 }]}>{dTail.item_name}</Text>
            <Text style={{ fontSize: 12 }}><Text style={styles.teksBold}>Current Price: </Text>{formatCurrency({ amount: dTail.current_price, code: "USD" })[0]}</Text>
            <Text style={{ fontSize: 12 }}><Text style={styles.teksBold}>Owner: </Text>{dTail.user.username}</Text>
            <Text style={{ fontSize: 12 }}><Text style={styles.teksBold}>Start bid: </Text>{getFormatedDate(new Date(dTail.post_date), "DD MMM YYYY")}</Text>
            <Text style={{ fontSize: 12 }}><Text style={styles.teksBold}>End bid: </Text>{getFormatedDate(new Date(dTail.end_date), "DD MMM YYYY")}</Text>
          </View>
          <View style={styles.bid}>
            <TextInput style={styles.input} keyboardType="numeric" onChangeText={setNumBid}></TextInput>
            <Button title='Bid' onPress={bidding}></Button>
          </View>
        </View>
        <View style={styles.bidBox}>
          <Text style={{ alignSelf: 'center' }}>Bidder List</Text>
          <ScrollView nestedScrollEnabled={true} >
            {
              mBidder.map(item =>
                <View style={styles.bidder} key={item.id}>
                  <Text>{item.user.username}</Text>
                  <Text>{formatCurrency({ amount: item.bid, code: "USD" })[0]}</Text>
                </View>
              )
            }
          </ScrollView>
        </View>
        <View style={styles.teks}>
          <Text style={styles.teksBold}>Description</Text>
          <Text>{dTail.description}</Text>
        </View>

      </View>
    </ScrollView>
  )
}

export default DetailItem

const styles = StyleSheet.create({
  box: {
    backgroundColor: '#66ccff',
    height: 'auto',
    // flexDirection: 'row',
    marginHorizontal: 10,
    marginVertical: 15,
    borderRadius: 8,
    paddingHorizontal: 30,
    paddingVertical: 5
    // flex: 1
  },
  image: {
    width: 150,
    height: 150,
    resizeMode: "contain",
    flex: 2,
    alignSelf: 'center',
    marginVertical: 10,
  },
  teks: {
    flex: 4,
    justifyContent: 'center',
    marginHorizontal: 5
    // backgroundColor: 'white'
  },
  bidBox: {
    height: 300,
    // marginHorizontal: 20,
    marginVertical: 10,
    backgroundColor: 'white',
    borderRadius: 8,
    paddingVertical: 15,
    paddingHorizontal: 10,
    flex: 1
  },
  bid: {
    flexDirection: 'row',
    marginTop: 10
  },
  input: {
    flex: 5,
    padding: 5,
    backgroundColor: 'white',
    borderBottomLeftRadius: 8,
    borderTopLeftRadius: 8
  },
  teksBold: {
    fontWeight: 'bold'
  },
  bidder: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderRadius: 8
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // marginTop: 22
},
modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    width: 200,
    borderWidth: 1,
    alignItems: "center",
    elevation: 5
},
modalText: {
    marginBottom: 15,
    textAlign: "center"
}

})