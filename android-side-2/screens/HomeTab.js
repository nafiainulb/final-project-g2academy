import { View, Text, Button } from 'react-native'
import React, { useEffect, useState } from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from './Home'
import MyItem from './MyItem'
import History from './History'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { instance as axios } from '../util/api';

import { FontAwesome, Entypo, Ionicons } from '@expo/vector-icons';
import Profile from './Profile';

import SockJS from 'sockjs-client';
import { over } from 'stompjs';

import { useSelector, useDispatch } from 'react-redux';
import { setStatusBarHidden } from 'expo-status-bar';

const Tab = createBottomTabNavigator();
var stompClient = null;

export default function HomeTab({ navigation }) {

  const [statuse, setStatuse] = useState(false)

  const message = useSelector(state => state.message);
  const mToken = useSelector(state => state.mToken);

  const dispact = useDispatch();

  const checkToken = async () => {
    const tokeni = await AsyncStorage.getItem('token');
    console.log("check token begin");
    if (tokeni) {
      setStatuse(true);
      console.log("token ada");
      connect();
      
      getHistory(tokeni);
      getMyItem(tokeni);
      aboutMe(tokeni);
      dispact({ type: "SET_TOKEN", payload: tokeni })
    };
  }

  const getAllBook = async () => {
    console.log("run get all item");
    try {
      const { data, status } = await axios.get("/api/product");
      if (status === 200) {
        // console.log("run get all item");
        dispact({ type: "SET_ITEM", payload: data })
      }
    } catch (err) {
      console.log(err);
    }
  }

  const getHistory = async (tok) => {
    console.log("run get history");
    try {
      const { data, status } = await axios.get("/api/myhistory", { headers: { Authorization: `Bearer ${tok}` } });
      if (status === 200) {
        dispact({ type: "SET_HISTORY", payload: data })
      }
    } catch (err) {
      console.log(err);
    }
  }

  const getMyItem = async (tok) => {
    console.log("run get me");
    try {
      const { data, status } = await axios.get("/api/product/myproduct", { headers: { Authorization: `Bearer ${tok}` } });
      if (status === 200) {
        // console.log("run get me");
        dispact({ type: "SET_MYITEM", payload: data })
      }
    } catch (err) {
      console.log(err);
    }
  }

  const aboutMe = async (tok) => {
    console.log("run about me");
    try {
      const { data, status } = await axios.get("/api/profile", { headers: { Authorization: `Bearer ${tok}` } });
      if (status === 200) {
        // console.log("run about me");
        // console.log(data);
        dispact({ type: "ADD_ACCOUNT", payload: data })
      }
    } catch (err) {
      console.log(err);
    }
  }

  // const getItem = async (id) => {
  //   try {
  //     const { data, status } = await axios.get(`/api/product/${id}`, { headers: { Authorization: `Bearer ${mToken}` } });
  //     if (status === 200) {
  //       dispact({ type: "SET_BIDDER", payload: data })
  //     }
  //   } catch (err) {
  //     console.log(err);
  //   }
  // }

  
  const connect = () => {
    let Sock = new SockJS('http://192.168.76.167:8080/ws');
    stompClient = over(Sock);
    stompClient.debug = () => {};
    console.log("run connect");
    stompClient.connect({}, onConnected, onError);
  }

  const onConnected = () => {
    console.log("subscribe");
    stompClient.subscribe('/topic/public', onMessageReceived);
    // userJoin();
  }

  const onError = (err) => {
    console.log("out error");
    console.log(err);
  }

  const onMessageReceived = (payload) => {
    var payloadData = JSON.parse(payload.body);
    console.log("receive");
    console.log(payloadData);
    dispact({ type: 'SET_MESSAGE', payload: payloadData })
  }

  const sendMessage = (id) => {
    var clientMessage = {
      action: "LOAD",
      payload: id
    };
    console.log("sending");
    stompClient.send("/app/message", {}, JSON.stringify(clientMessage));
  }

  useEffect(() => {
    console.log("phase 1");
    checkToken();
    getAllBook();
  }, [])

  useEffect(() => {
    console.log("recive message");
    if (message.action === "BEGIN") {
      console.log("Begin");
      sendMessage(message.payload);
      // checkToken();
    }
    if (message.action === "LOAD") {
      console.log("LOAD");
      getAllBook();
      getMyItem(mToken);
      getHistory(mToken);
    }
    if (message.action === "LOGIN") {
      console.log("Begin");
      checkToken();
    }
  }, [message])

  const logout = async () => {
    console.log("hapus token");
    await AsyncStorage.removeItem('token');
    setStatuse(false);
    navigation.navigate("Login");
    
  }

  const login = async () => {
    navigation.navigate("Login");
  }


  return (
    <Tab.Navigator screenOptions={{
      headerRight: () => (
        <View style={{ marginRight: 7 }}>
          {
            !statuse ?
              <Button
                onPress={login}
                title="Login"
              />
              :
              <Button
                onPress={logout}
                title="Logout"
              />
          }
        </View>
      ),
    }}>
      <Tab.Screen
        name='Home'
        component={Home}
        options={{
          tabBarIcon: ({ color, size }) => (<FontAwesome name="home" size={24} color="black" />)
        }}
      />
      {
        statuse && 
      <Tab.Screen
        name='My Item'
        component={MyItem}
        options={{
          tabBarIcon: ({ color, size }) => (<Entypo name="briefcase" size={24} color="black" />)
        }}
      /> }
      {
        statuse && 
      <Tab.Screen
        name='History'
        component={History}
        options={{
          tabBarIcon: ({ color, size }) => (<FontAwesome name="history" size={24} color="black" />)
        }}
      /> }
      {
        statuse && 
      <Tab.Screen
        name='Profile'
        component={Profile}
        options={{
          tabBarIcon: ({ color, size }) => (<Ionicons name="person" size={24} color="black" />)
        }}
      /> }
      
    </Tab.Navigator>
  )
}