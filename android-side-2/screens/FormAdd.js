import { Button, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import DatePicker, { getFormatedDate } from 'react-native-modern-datepicker';
import * as ImagePicker from 'expo-image-picker';
import { deleteObject, getDownloadURL, ref, uploadBytes } from 'firebase/storage';
import { storage } from '../firebase';
import { useDispatch, useSelector } from 'react-redux';
import { instance as axios } from '../util/api';

const FormAdd = ({ route, navigation }) => {

  const [open, setOpen] = useState(false);
  const dispact = useDispatch();

  const [name, setName] = useState('');
  const [desc, setDesc] = useState('');
  const [price, setPrice] = useState('');
  const [selectedDate, setSelectedDate] = useState('1/1/2020');
  const [image, setImage] = useState(null);
  const mToken = useSelector(state => state.mToken);

  const [uploadImage, setUploadImage] = useState(null);
  const [urlImage, setUrlImage] = useState(null);

  const addImage = async () => {
    let _image = await ImagePicker.launchImageLibraryAsync();
    if (!_image.cancelled) {
      setUploadImage(_image.uri);
      console.log(_image);
      console.log(detecttype(_image.uri));
    }
  }

  const sendData = () => {
    // const send = {item_name: name, description: desc, current_price: price,};
    // upload();
    // console.log(send);
    alert("still in building process");
    // detecttype(uploadImage.uri);
  }

  //--------------------------------------------------------------------------

  const addBook = async (temp, metode, kodek) => {
    // console.log("my item");
    console.log(temp);
    console.log(metode);
    console.log(kodek);
    console.log(mToken);

    console.log("run get history");
    try {
      if (metode === "POST") {
        console.log("going post");
        const { data, status } = await axios.post("/api/product", temp, { headers: { Authorization: `Bearer ${mToken}` } });
        if (status === 200) {
          dispact({ type: "ADD_ITEM", payload: data })
          dispact({ type: "SET_MESSAGE", payload: { action: "LOAD" } })
          navigation.navigate("HomeTab");
        }
      } else {
        console.log("going put");
        const { data, status } = await axios.put("/api/product", temp, { headers: { Authorization: `Bearer ${mToken}` } });
        if (status === 200) {
          dispact({ type: "UPDATE_ITEM", payload: data })
          dispact({ type: "SET_MESSAGE", payload: { action: "LOAD" } })
          navigation.navigate("HomeTab");
        }
      }
      // if (status === 200) {
      //   console.log('Finish Process');
      //   if (metode === 'POST') {
      //     dispact({ type: "ADD_ITEM", payload: data })
      //   } else {
      //     dispact({ type: "UPDATE_ITEM", payload: data })
      //   }
      //   // dispact({ type: "SET_MESSAGE", payload: { action: "LOAD", payload: route.params.id } })
      //   navigate('/myitem');
      // }
    } catch (err) {
      console.log(err);
    }
  }

  const deleted = () => {

    if (image !== null) {
      // Create a reference to the file to delete
      console.log("run delete");
      const desertRef = ref(storage, urlImage);

      // Delete the file
      deleteObject(desertRef).then(() => {
        console.log("delete image success");

        // File deleted successfully
      }).catch((error) => {
        console.log(error);
        // Uh-oh, an error occurred!
      });
    }

  }

  const upload = () => {

    let downloadUrl = image;
    console.log(downloadUrl);
    if (uploadImage !== null) {
      console.log("post with new image");
      getUrl();
      // deleted();
    } else if (urlImage) {
      console.log("put begin");
      const temp = { item_name: name, description: desc, image: downloadUrl };
      addBook(temp, 'PUT', route.params.id);
    } else {
      console.log("post begin");
      const temp = { item_name: name, current_price: price, description: desc, image: downloadUrl, end_date: getFormatedDate(new Date(selectedDate), "YYYY-MM-DD") };
      addBook(temp, 'POST', '');
    }
  }

  const getUrl = () => {
    // untuk storage
    console.log("r1");
    let fileName = uploadImage.split('/').pop();
    console.log("r2");
    const storageRef = ref(storage, `images/${fileName}`);
    console.log("r3");
    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, uploadImage, { contentType: detecttype(uploadImage), })
      .then(snapshot => {
        getDownloadURL(storageRef)
          .then(downloadUrl => {
            // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
            setUrlImage(downloadUrl);
            console.log("run upload");
            if (urlImage) {
              console.log("get put");
              const temp = { item_name: name, description: desc, image: downloadUrl };
              addBook(temp, 'PUT', route.params.id);
            } else {
              console.log("get post");
              const temp = { item_name: name, current_price: price, description: desc, image: downloadUrl, end_date: getFormatedDate(new Date(selectedDate), "YYYY-MM-DD") };
              addBook(temp, 'POST', '');
            }

          }).catch((error) => {
            console.log(error);
            // Uh-oh, an error occurred!
          })

      })
      .catch(err => {
        console.log(err);
      })
  }

  const detecttype = (url) => {
    const tipe2 = url.split(".").pop();
    if (tipe2 === "png") {
      return "image/png"
    }
    if (tipe2 === "jpg" || tipe2 === "jpeg") {
      return "image/jpeg"
    }
  }
  //--------------------------------------------------------------------------

  useEffect(() => {
    console.log(`n${route.param}n`);
    if (route.params) {
      setName(route.params.item_name);
      setDesc(route.params.description);
      setImage(route.params.image);
    } else {
      console.log("empty detected");
    }
  }, [])

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.teksUp}>Item Name</Text>
      <TextInput style={styles.dateinput} onChangeText={setName} placeholder="Item Name" autoCapitalize='none' />
      <Text style={styles.teksUp}>Description</Text>
      <TextInput style={styles.dateinput} onChangeText={setDesc} placeholder="Description" autoCapitalize='none' multiline numberOfLines={4} />
      {
        !route.params &&
        <View>
          <Text style={styles.teksUp}>Initial Price</Text>
          <TextInput style={styles.dateinput} keyboardType="numeric" onChangeText={setPrice} placeholder="Initial Price" />
          <Text style={styles.teksUp}>End Bidding Date</Text>
          <View style={styles.boxdate}>
            <TextInput style={styles.dateinput} value={getFormatedDate(new Date(selectedDate), "DD MMM YYYY")}  />
            <Button style={styles.button} title={open ? "close" : "open"} onPress={() => setOpen(!open)} />
          </View>
        </View>
      }
      {
        open &&
        <DatePicker
          onSelectedChange={date => setSelectedDate(date)}
          mode='calendar'
        />
      }
      <View style={styles.button}>
        <Button title="Choose Photo" onPress={addImage} />
      </View>
      <View style={styles.button}>
        <Button title='Submit' onPress={sendData} />
      </View>
    </ScrollView>
  )
}

export default FormAdd

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  teksUp: {
    marginTop: 5
  },
  dateinput: {
    flex: 5,
    padding: 5,
    backgroundColor: 'white'
  },
  boxdate: {
    flexDirection: 'row'
  },
  button: {
    marginVertical: 5
  }
})