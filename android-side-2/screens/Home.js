import { View, Text, StyleSheet, ScrollView, FlatList } from 'react-native'
import React, { useEffect, useState } from 'react'
import AuctionItem from '../components/AuctionItem';
import { useSelector } from 'react-redux';

const Home = ({ navigation }) => {

  const itemList = useSelector(state => state.itemList);

  return (
    // <ScrollView>
      <View style={styles.container}>
        <FlatList
          data={itemList}
          renderItem={({ item }) => <AuctionItem item={item} navigation={navigation} />}
        />
      </View>
    // </ScrollView>
  )
}

export default Home

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
})