import { View, Text, StyleSheet, Button, ScrollView, FlatList } from 'react-native'
import React from 'react'
import { useSelector } from 'react-redux';
import AuctionItem from '../components/AuctionItem';

const MyItem = ({ navigation }) => {

  const mItem = useSelector(state => state.mItem);

  const gotoAdd = () => {
    navigation.navigate("Add Item");
  }

  return (
    // <ScrollView>
      <View style={styles.container}>
        <Button title='Add Item' onPress={gotoAdd}></Button>
        <FlatList
          data={mItem}
          renderItem={({ item }) => <AuctionItem item={item} navigation={navigation} />}
        />
      </View>
    // </ScrollView>
  )
}

export default MyItem

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
})