import { ActivityIndicator, Alert, Button, Modal, Pressable, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native'
import React, { useState } from 'react'
import DatePicker, { getFormatedDate } from 'react-native-modern-datepicker';
import Picker from '@ouroboros/react-native-picker';
import { instance as axios } from '../util/api';

import { useSelector, useDispatch } from 'react-redux';

const Profile = () => {
    const userAccount = useSelector(state => state.userAccount);
    const mToken = useSelector(state => state.mToken);
    const dispact = useDispatch();

    const [selectedDate, setSelectedDate] = useState(userAccount.birthDate);
    const [open, setOpen] = useState(false);
    const [gender, setgender] = useState(userAccount.gender);
    const [name, setUsername] = useState(userAccount.short_name);
    const [maddress, setPassword] = useState(userAccount.address);
    const [modalVisible, setModalVisible] = useState(false);
    const [result, setResult] = useState('');

    const editProfile = async () => {
        const temp = { short_name: name, birthDate: selectedDate, gender: gender, address: maddress }
        setResult('');
        setModalVisible(!modalVisible);
        console.log(temp);
        console.log("run");
        try {
            const { data, status } = await axios.put("/api/profile", temp, { headers: { Authorization: `Bearer ${mToken}` } });
            if (status === 200) {
                console.log("done");
                setResult("Update Profile Done");
                dispact({ type: 'ADD_ACCOUNT', payload: temp })
            }
        } catch (err) {
            setResult("Update Profile Fail");
        }
    }

    const Popup = () => {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        {
                            result === '' ?
                                <View>
                                    <Text style={styles.modalText}>Loading</Text>
                                    <ActivityIndicator />
                                </View>
                                :
                                <View>
                                    <Text style={styles.modalText}>{result}</Text>
                                    <Button title='Close' onPress={() => setModalVisible(!modalVisible)} />
                                </View>
                        }
                    </View>
                </View>
            </Modal>
        )
    }

    return (
        <View style={styles.container}>
            <Popup/>
            <ScrollView>
                <Text style={styles.teksUp}>Short Name</Text>
                <TextInput style={styles.dateinput} onChangeText={setUsername} value={name} placeholder="Name" autoCapitalize='none' />
                <Text style={styles.teksUp}>Address</Text>
                <TextInput style={styles.dateinput} onChangeText={setPassword} value={maddress} placeholder="Address" autoCapitalize='none' />
                <Text style={styles.teksUp}>Birth Date</Text>
                <View style={styles.boxdate}>
                    <TextInput style={styles.dateinput} value={getFormatedDate(new Date(selectedDate), "DD MMM YYYY")} />
                    <Button style={styles.button} title={open ? "close" : "open"} onPress={() => setOpen(!open)} />
                </View>
                {
                    open &&
                    <DatePicker
                        onSelectedChange={date => setSelectedDate(date)}
                        mode='calendar'
                    />
                }
                <Text style={styles.teksUp}>Gender</Text>
                <Picker
                    onChanged={setgender}
                    options={[
                        { value: 'male', text: 'Male' },
                        { value: 'female', text: 'Female' }
                    ]}
                    style={{ backgroundColor: 'white', padding: 4 }}
                    value={gender}
                />
                <View style={styles.button}>
                    <Button title='Submit' onPress={editProfile} />
                </View>
                {/* <Button title='modal' onPress={() => setModalVisible(!modalVisible)} /> */}

            </ScrollView>
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 10,
        marginVertical: 8
    },
    boxdate: {
        flexDirection: 'row'
    },
    dateinput: {
        flex: 5,
        padding: 5,
        backgroundColor: 'white'
    },
    button: {
        marginTop: 10
    },
    teksUp: {
        marginTop: 5
    },

    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        // marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        width: 200,
        borderWidth: 1,
        alignItems: "center",
        elevation: 5
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }

})