import axios from 'axios';

export const instance = axios.create({
    baseURL: "http://192.168.76.167:8080",
    headers: {
        "Content-Type": "application/json"
    }
})
