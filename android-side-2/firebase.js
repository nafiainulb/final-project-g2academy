// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyC_7D72yXoVVDqJgq_O-fRXXr4eNOeuDwo",
  authDomain: "book-store-datastore.firebaseapp.com",
  projectId: "book-store-datastore",
  storageBucket: "book-store-datastore.appspot.com",
  messagingSenderId: "111238453759",
  appId: "1:111238453759:web:067d1dadf34856971b6fca"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize Cloud Firestore and get a reference to the service
export const db = getFirestore(app);
// Initialize Cloud Storage and get a reference to the service
export const storage = getStorage(app);