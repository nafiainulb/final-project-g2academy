import { Button, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { formatCurrency } from 'react-native-format-currency';
import { getFormatedDate } from 'react-native-modern-datepicker';

const HistoryItem = ({ item, navigation }) => {

  // const gotoDetail = () => {
  //   navigation.navigate("Detail Item");
  // }

  return (
    <TouchableOpacity>
      <View style={styles.box}>
        <Image source={{ uri: item.image }} style={styles.image} />
        <View style={styles.teks}>
          <Text style={[styles.teksBold, { fontSize: 13 }]}>{item.item_name}</Text>
          <Text style={{ fontSize: 12 }}><Text style={styles.teksBold}>Current Price: </Text>{formatCurrency({ amount: item.bid , code: "USD" })[0]}</Text>
          {/* <Text style={{ fontSize: 12 }}><Text style={styles.teksBold}>Owner: </Text>{item.user.username}</Text> */}
          {/* <Text style={{ fontSize: 12 }}><Text style={styles.teksBold}>End bid: </Text>{getFormatedDate(new Date(item.end_date), "DD MMM YYYY")}</Text> */}
        </View>
        <View style={styles.button}>
          <Button title='Bidding' disabled></Button>
        </View>
      </View>
    </TouchableOpacity>
  )
}

export default HistoryItem

const styles = StyleSheet.create({
  box: {
    // backgroundColor: '#66ccff',
    height: 'auto',
    flexDirection: 'row',
    marginHorizontal: 5,
    marginVertical: 4,
    borderRadius: 8,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderWidth: 3,
  },
  image: {
    width: 100,
    height: 100,
    resizeMode: "contain",
    flex: 2,
  },
  teks: {
    flex: 4,
    justifyContent: 'center',
    marginHorizontal: 5
    // backgroundColor: 'white'
  },
  button: {
    flex: 2.5,
    justifyContent: 'center'
  },
  teksBold: {
    fontWeight: 'bold'
  }
})