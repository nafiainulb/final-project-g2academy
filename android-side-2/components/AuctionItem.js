import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { formatCurrency } from 'react-native-format-currency';
import { getFormatedDate } from 'react-native-modern-datepicker';

const AuctionItem = ({item, navigation }) => {

    const gotoDetail = () => {
        navigation.navigate("Detail Item", item);
    }

    return (
        <TouchableOpacity onPress={gotoDetail}>
            <View style={styles.box}>
                <Image source={{ uri: item.image }} style={styles.image} />
                <View style={styles.teks}>
                    <Text style={styles.teksBold}>{item.item_name}</Text>
                    <Text><Text style={styles.teksBold}>Current Price: </Text>{formatCurrency({amount: item.current_price, code: "USD" })[0]}</Text>
                    <Text><Text style={styles.teksBold}>Owner: </Text>{item.user.username}</Text>
                    <Text><Text style={styles.teksBold}>End bid: </Text>{getFormatedDate(new Date(item.end_date), "DD MMM YYYY")}</Text>
                </View>
            </View>
        </TouchableOpacity>

    )
}

export default AuctionItem

const styles = StyleSheet.create({
    box: {
        // backgroundColor: '#66ccff',
        height: 'auto',
        flexDirection: 'row',
        marginHorizontal: 5,
        marginVertical: 4,
        borderRadius: 8,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderWidth: 3,
    },
    image: {
        width: 100,
        height: 100,
        resizeMode: "contain",
        flex: 2,
        marginHorizontal: 8
    },
    teks: {
        flex: 3,
        justifyContent: 'center'
        // backgroundColor: 'white'
    },
    teksBold: {
        fontWeight: 'bold'
    }
})