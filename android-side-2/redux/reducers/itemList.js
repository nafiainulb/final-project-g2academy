const itemList = (state = [], action) => {
    switch (action.type) {
        case "SET_ITEM":
            return action.payload;
        // break;
        case "DELETE_ITEM":
            return state.filter(book => {
                if (book.id !== action.payload) {
                    return book;
                }
            });
        // break;
        case "ADD_ITEM":
            return [...state, action.payload];
        case "UPDATE_ITEM":
            return state.map(book => {
                if (book.id === action.payload.id) {
                    return action.payload;
                } else {
                    return book;
                }
            });
        default:
            return state;
        // break;

    }
}

export default itemList;