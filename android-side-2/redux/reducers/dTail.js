const dTail = (state = {item_name: '', description: '', current_price: 0, image: 'https://firebasestorage.googleapis.com/v0/b/book-store-datastore.appspot.com/o/images%2Fcovid_batu%20-%202022-03-31.jpg?alt=media&token=c633f740-8031-4e23-a052-2631c1cc3b99', post_date: '1111/1/1', end_date: '1111/1/2', user: {username: ''}}, action) => {
    switch (action.type) {
        case "SET_DETAIL":
            return action.payload;
        // break;
        case "DELETE_DETAIL":
            return state.filter(book => {
                if (book.id !== action.payload) {
                    return book;
                }
            });
        // // break;
        // case "ADD_BIDDER":
        //     return [...state, action.payload];
        // case "UPDATE_BIDDER":
        //     return state.map(book => {
        //         if (book.id === action.payload.id) {
        //             return action.payload;
        //         } else {
        //             return book;
        //         }
        //     });
        default:
            return state;
        // break;

    }
}

export default dTail;