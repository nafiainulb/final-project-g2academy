import { combineReducers } from "redux";
import message from "./message";
import userAccount from "./userAccount";
import mItem from "./mItem";
import itemList from "./itemList";
import mHistory from "./mHistory";
import mToken from "./mToken";
import mBidder from "./mBidder";
import dTail from "./dTail";

const reducers = combineReducers({
    message: message,
    userAccount : userAccount,
    itemList: itemList,
    mItem: mItem,
    mHistory: mHistory,
    mToken: mToken,
    mBidder: mBidder,
    dTail: dTail
})

export default reducers;