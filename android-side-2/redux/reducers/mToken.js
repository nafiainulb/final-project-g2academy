const mToken = (state = '', action) => {
    switch (action.type) {
        case "SET_TOKEN":
            return action.payload;
        // break;
        case "DELETE_TOKEN":
            return state.filter(book => {
                if (book.id !== action.payload) {
                    return book;
                }
            });
        default:
            return state;
        // break;

    }
}

export default mToken;