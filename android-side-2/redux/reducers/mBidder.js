const mBidder = (state = [], action) => {
    switch (action.type) {
        case "SET_BIDDER":
            return action.payload;
        // break;
        case "DELETE_BIDDER":
            return state.filter(book => {
                if (book.id !== action.payload) {
                    return book;
                }
            });
        // // break;
        // case "ADD_BIDDER":
        //     return [...state, action.payload];
        // case "UPDATE_BIDDER":
        //     return state.map(book => {
        //         if (book.id === action.payload.id) {
        //             return action.payload;
        //         } else {
        //             return book;
        //         }
        //     });
        default:
            return state;
        // // break;

    }
}

export default mBidder;