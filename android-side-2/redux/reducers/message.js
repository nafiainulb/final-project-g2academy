const message = (state = '', action) => {
    switch (action.type) {
        case "SET_MESSAGE":
            return action.payload;
        // break;
        case "DELETE_MESSAGE":
            return state.filter(book => {
                if (book.id !== action.payload) {
                    return book;
                }
            });
        // break;
        // case "ADD_MESSAGE":
        //     return [...state, action.payload];
        // case "UPDATE_MESSAGE":
        //     return state.map(book => {
        //         if (book.id === action.payload.id) {
        //             return action.payload;
        //         } else {
        //             return book;
        //         }
        //     });
        default:
            return state;
        // break;

    }
}

export default message;