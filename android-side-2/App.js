
import React from 'react';
import Login from './screens/Login';
import Regis from './screens/Regis';
import HomeTab from './screens/HomeTab';
import DetailItem from './screens/DetailItem';
import FormAdd from './screens/FormAdd';
import store from './redux/store';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Provider } from 'react-redux';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Provider store={store}>
        <Stack.Navigator>
          <Stack.Screen name="HomeTab" component={HomeTab} options={{ headerShown: false }} />
          <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
          <Stack.Screen name="Register" component={Regis} />
          <Stack.Screen name="Detail Item" component={DetailItem} />
          <Stack.Screen name="Add Item" component={FormAdd} />
        </Stack.Navigator>
      </Provider>
    </NavigationContainer>
  );
}


