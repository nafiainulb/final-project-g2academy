package com.finalProject.dao;

import com.finalProject.model.DAOUser;
import com.finalProject.model.DaoProfile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileDao extends CrudRepository<DaoProfile, Long> {
    DaoProfile findByDaoUser(DAOUser daoUser);
}
