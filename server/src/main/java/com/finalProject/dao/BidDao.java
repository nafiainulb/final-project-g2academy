package com.finalProject.dao;

import com.finalProject.model.DAOBid;
import com.finalProject.model.DAOUser;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BidDao extends CrudRepository<DAOBid, Long> {
    List<DAOBid> findByItemId(long id);
    List<DAOBid> findByItemUser(DAOUser user);

    List<DAOBid> findByUser(DAOUser user);

}
