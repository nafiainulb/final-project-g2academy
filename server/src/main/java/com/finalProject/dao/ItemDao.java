package com.finalProject.dao;

import com.finalProject.model.DAOItem;
import com.finalProject.model.DAOUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemDao extends CrudRepository<DAOItem, Long> {
    List<DAOItem> findByUser (DAOUser user);
}
