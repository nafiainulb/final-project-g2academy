package com.finalProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class auctionMain {

    public static void main(String[] args) {
        SpringApplication.run(auctionMain.class, args);
    }
}
