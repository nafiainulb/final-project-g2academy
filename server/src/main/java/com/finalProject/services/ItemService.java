package com.finalProject.services;

import com.finalProject.dao.BidDao;
import com.finalProject.model.DAOBid;
import com.finalProject.model.ItemDTO;
import com.finalProject.model.DAOItem;
import com.finalProject.model.DAOUser;
import com.finalProject.dao.ItemDao;
import com.finalProject.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class ItemService {

    public static final Logger logger = LoggerFactory.getLogger(ItemService.class);
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private UserDao userDao;

    public List<DAOItem> getItemAll() {
       return  (List<DAOItem>) itemDao.findAll();
    }

    public DAOItem getItemById(long id) {
        return itemDao.findById(id).get();
    }

    public List<DAOItem> getMyItem(String username) {
        DAOUser user = userDao.findByUsername(username);
        return itemDao.findByUser(user);
    }

    public DAOItem saveItem(String username, ItemDTO item) {
        DAOUser user = userDao.findByUsername(username);
        DAOItem newItem = new DAOItem();
        newItem.setItem_name(item.getItem_name());
        newItem.setCurrent_price(item.getCurrent_price());
        newItem.setDescription(item.getDescription( ));
        newItem.setImage(item.getImage());
        newItem.setPost_date(timestamp);
        newItem.setEnd_date(item.getEnd_date());
        newItem.setUser(user);
        return itemDao.save(newItem);
    }

    public DAOItem updateItem(long id, String username, ItemDTO item ) {
        DAOUser user = userDao.findByUsername(username);
        DAOItem setitem = getItemById(id);
        if ( setitem.getUser().getUsername().equals(user.getUsername()) ) {
            setitem.setItem_name(item.getItem_name());
            setitem.setCurrent_price(item.getCurrent_price());
            setitem.setDescription(item.getDescription( ));
            setitem.setImage(item.getImage());
            setitem.setPost_date(timestamp);
            setitem.setEnd_date(item.getEnd_date());
            return itemDao.save(setitem);
        } else {
            return null;
        }
    }

    public void deleteItem(long id, String username) {
        DAOUser user = userDao.findByUsername(username);
        DAOItem setitem = getItemById(id);
        if ( setitem.getUser().getUsername().equals(user.getUsername()) ) {
            itemDao.deleteById(id);
        }
    }
}
