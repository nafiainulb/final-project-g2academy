package com.finalProject.services;

import com.finalProject.dao.ProfileDao;
import com.finalProject.dao.UserDao;
import com.finalProject.model.DAOUser;
import com.finalProject.model.DaoProfile;
import com.finalProject.model.ProfileDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileService {

    @Autowired
    private ProfileDao profileDao;

    @Autowired
    private UserDao userDao;

    public void addprofile (DAOUser user) {
        DaoProfile newprofile = new DaoProfile();
        newprofile.setUser(user);
        profileDao.save(newprofile);
    }

    public DaoProfile getProfile (String username) {
        DAOUser setUser = userDao.findByUsername(username);
        return profileDao.findByDaoUser(setUser);
    }

    public DaoProfile updateProfile (ProfileDTO profile, String username) {
        DaoProfile profileUser = getProfile(username);
        profileUser.setAddress(profile.getAddress());
        profileUser.setGender(profile.getGender());
        profileUser.setShort_name(profile.getShort_name());
        profileUser.setBirthDate(profile.getBirthDate());
        return profileDao.save(profileUser);
    }

}
