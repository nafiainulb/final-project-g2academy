package com.finalProject.services;

import com.finalProject.dao.UserDao;
import com.finalProject.model.DAOUser;
import com.finalProject.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<SimpleGrantedAuthority> roles = null;
		DAOUser user = userDao.findByUsername(username);
		if(user.getRole().equals("admin"))
		{
			roles= Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
			return new User(user.getUsername(),user.getPassword(),roles);
		}
		if(user.getRole().equals("user"))
		{
			roles=Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
			return new User(user.getUsername(),user.getPassword(),roles);
		}
		throw new UsernameNotFoundException("User not found with the name "+ username);

	}

	public DAOUser save(UserDTO user) {
		DAOUser temp = userDao.findByUsername(user.getUsername( ) );
		if (temp == null) {
			DAOUser newUser = new DAOUser();
			newUser.setUsername(user.getUsername());
			newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
			return userDao.save(newUser);
		} else {
			return null;
		}
	}
}