package com.finalProject.services;

import com.finalProject.dao.BidDao;
import com.finalProject.dao.ItemDao;
import com.finalProject.dao.UserDao;
import com.finalProject.model.BidDTO;
import com.finalProject.model.DAOBid;
import com.finalProject.model.DAOItem;
import com.finalProject.model.DAOUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class BidService {

    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    @Autowired
    private BidDao bidDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ItemDao itemDao;

    public String saveBid(long id, String username, BidDTO bid) {

        DAOItem current = itemDao.findById(id).get();
        if (current.getCurrent_price() < bid.getBid( )) {
            if (current.getEnd_date().after(timestamp)) {
                DAOUser user = userDao.findByUsername(username);

                List<DAOBid> listItem = bidDao.findByUser(user);
                for (DAOBid item:listItem) {
                    if (item.getItem().getId() == id) {
                        bidDao.deleteById(item.getId());
//                        break;
                    }
                }
                DAOBid newBid = new DAOBid();
                current.setCurrent_price(bid.getBid());
                newBid.setItem(current);
                newBid.setBid(bid.getBid());
                newBid.setCreate_date(timestamp);
                newBid.setUser(user);
                bidDao.save(newBid);
                return "Bid Success";
            } else {
                return "Bid already end";
            }
        } else {
            return "Bid Fail";
        }
    }

    public List<DAOBid> bidderUser(long id) {
        return bidDao.findByItemId(id);
    }

    public List<BidDTO> history(String username) {
        DAOUser user = userDao.findByUsername(username);
        List<DAOBid> listItem = bidDao.findByUser(user);
        List<BidDTO> sendItem = new ArrayList<>();
        for ( int i = 0; i < listItem.size() ; i++ ) {
            BidDTO temp = new BidDTO();
            temp.setId(listItem.get(i).getId());
            temp.setBid(listItem.get(i).getBid());
            temp.setCreate_date(listItem.get(i).getCreate_date());
            temp.setImage(listItem.get(i).getItem().getImage());
            temp.setItem_name(listItem.get(i).getItem().getItem_name());
            sendItem.add(temp);
        }
        return sendItem;
    }

    public void resetBid(DAOItem item){
        if (item.getEnd_date().before(timestamp)) {
            List<DAOBid> listbid = bidDao.findByItemId(item.getId());
            for ( int i = 0; i < listbid.size()-1; i++ ) {
                listbid.get(i).setStatus("LOSE");
            }
            listbid.get(listbid.size()).setStatus("WIN");
            bidDao.saveAll(listbid);
        }
    }
}
