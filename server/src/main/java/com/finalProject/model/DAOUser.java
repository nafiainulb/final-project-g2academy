package com.finalProject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class DAOUser {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column
	private String username;
	@Column
	@JsonIgnore
	private String password;
	@JsonIgnore
	private String role = "user";
//	@OneToOne(mappedBy = "daouser")

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

//	@OneToMany(mappedBy = "users")
//	private Set<DaoTransaction> userTranssaction;

//	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY,
//			cascade = CascadeType.ALL)
//	private Set<DaoTransaction> daoTransactions;
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}


}