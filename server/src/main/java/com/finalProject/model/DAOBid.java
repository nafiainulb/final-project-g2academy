package com.finalProject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "bid")
public class DAOBid {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long bid;

    private Date create_date;
    private String status = "WAIT";

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "item_id", nullable = false, referencedColumnName = "id")
    private DAOItem item;

    @ManyToOne
    @JoinColumn(name = "bidder_id", nullable = false, referencedColumnName = "id")
    private DAOUser user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBid() {
        return bid;
    }

    public void setBid(long bid) {
        this.bid = bid;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public DAOItem getItem() {
        return item;
    }

    public void setItem(DAOItem item) {
        this.item = item;
    }

    public DAOUser getUser() {
        return user;
    }

    public void setUser(DAOUser user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
