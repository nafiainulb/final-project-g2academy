package com.finalProject.model;

import java.util.Date;
import java.util.List;

public class ItemDTO {

    private String item_name;

    private String image;

    private String description;

    private long current_price;

    private Date post_date;

    private Date end_date;

    private DAOUser user;

    private List<DAOBid> bidder;

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCurrent_price() {
        return current_price;
    }

    public void setCurrent_price(long current_price) {
        this.current_price = current_price;
    }

    public Date getPost_date() {
        return post_date;
    }

    public void setPost_date(Date post_date) {
        this.post_date = post_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public DAOUser getUser() {
        return user;
    }

    public void setUser(DAOUser user) {
        this.user = user;
    }

    public List<DAOBid> getBidder() {
        return bidder;
    }

    public void setBidder(List<DAOBid> bidder) {
        this.bidder = bidder;
    }
}
