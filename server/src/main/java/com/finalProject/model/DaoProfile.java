package com.finalProject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.bytebuddy.utility.RandomString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name="profile")
public class DaoProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private long id;
    private String address = "";
    private String gender = "";
    private Date birthDate = new Date(  );
    private String short_name = RandomString.make( 8 );

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "daouser_id")
    @JsonIgnore
    private DAOUser daoUser;

    public DAOUser getUser() {
        return daoUser;
    }

    public void setUser(DAOUser user) {
        this.daoUser = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

}
