package com.finalProject.controller;

import com.finalProject.config.JwtTokenUtil;
import com.finalProject.model.*;
import com.finalProject.services.JwtUserDetailsService;
import com.finalProject.services.ProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
	private ProfileService profileService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);

//		HashMap<String, String> mapper = new HashMap<>();
//		mapper.put("token",token);
//		mapper.put("role",userDetails.getAuthorities().toString());

		return ResponseEntity.ok(new JwtResponse(token));
//		return ResponseEntity.ok(mapper);
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> saveUser(@RequestBody UserDTO user) throws Exception {
		DAOUser newUser = userDetailsService.save(user);
		profileService.addprofile(newUser);
		if (newUser != null) {
			logger.info("ok");
			return ResponseEntity.ok(newUser);
		} else {
			logger.info("fail");
			return new ResponseEntity<>(HttpStatus.ALREADY_REPORTED);
		}
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}