package com.finalProject.controller;

import com.finalProject.config.JwtRequestFilter;
import com.finalProject.config.JwtTokenUtil;
import com.finalProject.dao.UserDao;
import com.finalProject.model.*;
import com.finalProject.services.BidService;
import com.finalProject.services.ItemService;
import com.finalProject.services.ProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class RestApiController {

    public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

    @Autowired
    private ItemService itemService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private BidService bidService;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @GetMapping(value = "/product", produces = "application/json")
    public ResponseEntity<?> getAllProduct() {
        List<DAOItem> listItem = itemService.getItemAll();
        return new ResponseEntity<>(listItem, HttpStatus.OK);
    }

    @GetMapping(value = "/product/{id}", produces = "application/json")
    public ResponseEntity<?> getProductById (@PathVariable("id") long id) {
        DAOItem listItem = itemService.getItemById(id);
        return new ResponseEntity<>(listItem, HttpStatus.OK);
    }

    @GetMapping(value = "/product/myproduct", produces = "application/json")
    public ResponseEntity<?> getMyProduct (HttpServletRequest request) {
        String username = usernameFromToken(request);
        List<DAOItem> listItem = itemService.getMyItem(username);
        return new ResponseEntity<>(listItem, HttpStatus.OK);
    }

    @PostMapping("/product")
    public ResponseEntity<?> saveProduct(HttpServletRequest request, @RequestBody ItemDTO item) {
        String username = usernameFromToken(request);
        DAOItem newItem = itemService.saveItem(username,item);
        return new ResponseEntity<>(newItem, HttpStatus.OK);
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<?> updateProduct(HttpServletRequest request,@PathVariable("id") long id, @RequestBody ItemDTO item) {
        String username = usernameFromToken(request);
        DAOItem newItem = itemService.updateItem(id,username,item);
        return new ResponseEntity<>(newItem, HttpStatus.OK);
    }

    @DeleteMapping("/product/{id}")
    public ResponseEntity<?> deleteProduct(HttpServletRequest request, @PathVariable("id") long id) {
        String username = usernameFromToken(request);
        itemService.deleteItem(id, username);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/bid/{id}")
    public ResponseEntity<?> saveBid(HttpServletRequest request,@PathVariable("id") long id, @RequestBody BidDTO bid) {
        logger.info(String.valueOf(bid.getBid()));
        logger.info(String.valueOf(id));
        String username = usernameFromToken(request);
        String response = bidService.saveBid(id,username,bid);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/bidder/{id}", produces = "application/json")
    public ResponseEntity<?> getBidderItemId (@PathVariable("id") long id) {
        List<DAOBid> listBidder = bidService.bidderUser(id);
        return new ResponseEntity<>(listBidder, HttpStatus.OK);
    }

    @GetMapping(value = "/myhistory", produces = "application/json")
    public ResponseEntity<?> getHistory (HttpServletRequest request) {
        String username = usernameFromToken(request);
        List<BidDTO> listBidder = bidService.history(username);
        return new ResponseEntity<>(listBidder, HttpStatus.OK);
    }

    @GetMapping(value = "/aboutme", produces = "application/json")
    public ResponseEntity<?> aboutMe (HttpServletRequest request) {
        String token = jwtRequestFilter.extractJwtFromRequest(request);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        String role = String.valueOf(jwtTokenUtil.getRolesFromToken(token));
        DaoProfile currentProfile = profileService.getProfile(username);
        HashMap<String, String> export = new HashMap<>();
        export.put("username", username);
        export.put("name", currentProfile.getShort_name());
        export.put("role",role);
        return new ResponseEntity<>(export, HttpStatus.OK);
    }

    // -------------------Get Profile-------------------------------------------
    @GetMapping("/profile")
    public ResponseEntity<?> getProfile(HttpServletRequest request) {
        String useri = usernameFromToken(request);
        DaoProfile currentProfile = profileService.getProfile(useri);
        return new ResponseEntity<>(currentProfile,HttpStatus.OK);
    }

    // -------------------Update Profile-------------------------------------------
    @PutMapping("/profile")
    public ResponseEntity<?> updatingProfile(HttpServletRequest request, @RequestBody ProfileDTO profile){
        String useri = usernameFromToken(request);
        DaoProfile updatedProfile = profileService.updateProfile(profile,useri);
        logger.info("success");
        return new ResponseEntity<>(updatedProfile,HttpStatus.OK);
    }

    @Scheduled(cron = "0 0 * * * ?")
    public void resetItem() {
        List<DAOItem> listItem = itemService.getItemAll();
        for ( DAOItem item:listItem) {
            bidService.resetBid(item);
        }
    }

    private String usernameFromToken(HttpServletRequest request) {
        String token = jwtRequestFilter.extractJwtFromRequest(request);
        return jwtTokenUtil.getUsernameFromToken(token);
    }
}
