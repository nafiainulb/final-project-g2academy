package com.finalProject.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.finalProject.model.DAOItem;
import com.finalProject.model.Message;
import com.finalProject.services.ItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ChatController {

    public static final Logger logger = LoggerFactory.getLogger(ChatController.class);

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private ItemService itemService;

    @MessageMapping("/message")
    @SendTo("/topic/public")
    public Message receiveMessage(@Payload Message message) throws JsonProcessingException {

        return message;
    }



}
