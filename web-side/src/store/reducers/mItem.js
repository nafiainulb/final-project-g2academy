const mItem = (state = [], action) => {
    switch (action.type) {
        case "SET_MYITEM":
            return action.payload;
        // break;
        case "DELETE_MYITEM":
            return state.filter(book => {
                if (book.id !== action.payload) {
                    return book;
                }
            });
        // break;
        case "ADD_MYITEM":
            return [...state, action.payload];
        case "UPDATE_MYITEM":
            return state.map(book => {
                if (book.id === action.payload.id) {
                    return action.payload;
                } else {
                    return book;
                }
            });
        default:
            return state;
        // break;

    }
}

export default mItem;