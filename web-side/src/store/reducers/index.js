import { combineReducers } from "redux";
import itemList from "./itemList";
import mbidder from "./mbidder";
import message from "./message";
import mHistory from "./mHistory";
import mItem from "./mItem";
import userAccount from "./userAccount";

const reducers = combineReducers({
    userAccount : userAccount,
    itemList: itemList,
    mItem: mItem,
    mHistory: mHistory,
    message: message,
    mbidder: mbidder
})

export default reducers;