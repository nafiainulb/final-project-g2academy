const mHistory = (state = [], action) => {
    switch (action.type) {
        case "SET_HISTORY":
            return action.payload;
        // break;
        case "DELETE_HISTORY":
            return state.filter(book => {
                if (book.id !== action.payload) {
                    return book;
                }
            });
        // break;
        case "ADD_HISTORY":
            return [...state, action.payload];
        case "UPDATE_HISTORY":
            return state.map(book => {
                if (book.id === action.payload.id) {
                    return action.payload;
                } else {
                    return book;
                }
            });
        default:
            return state;
        // break;

    }
}

export default mHistory;