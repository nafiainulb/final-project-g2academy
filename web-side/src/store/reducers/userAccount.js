const data = '';

const userAccount = (state = data, action) => {
    switch(action.type) {
        case "DELETE_ACCOUNT":
            return '';
            // break;
        case "ADD_ACCOUNT":
            return action.payload;
            // break;
        default:
            return state;
            // break;
    }
}

export default userAccount;