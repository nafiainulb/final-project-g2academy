import React, { useEffect, useState } from 'react'
import { Route, Routes } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import Footer from './component/Footer'
import MyNavbar from './component/MyNavbar'
import PrivateRoute from './component/PrivateRoute'
import DetailItem from './page/DetailItem'
import History from './page/History'
import Home from './page/Home'
import InputForm from './page/InputForm'
import Login from './page/Login'
import MyItem from './page/MyItem'
import Profile from './page/Profile'
import Register from './page/Register'
import SockJS from 'sockjs-client';
import { over } from 'stompjs';

var stompClient = null;
function App() {

  const [stat, setStat] = useState(0);

  const dispact = useDispatch();

  const getAllBook = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/product`);
      if (response.status !== 200) {
        throw Error('something wrong')
      } else {
        const data = await response.json();
        dispact({ type: "SET_ITEM", payload: data })
      }
    } catch (err) {
      console.log(err);
    }
  }

  const getHistory = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/myhistory`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
      if (response.status !== 200) {
        throw Error('something wrong')
      } else {
        const data = await response.json();
        dispact({ type: "SET_HISTORY", payload: data })
      }
    } catch (err) {
      console.log(err);
    }
  }

  const getMyItem = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/product/myproduct`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
      if (response.status !== 200) {
        throw Error('something wrong')
      } else {
        const data = await response.json();
        console.log(`my item : ${data}`);
        dispact({ type: "SET_MYITEM", payload: data })
      }
    } catch (err) {
      console.log(err);
    }
  }

  const aboutMe = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/aboutme`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
      if (response.status !== 200) {
        throw Error('something wrong')
      } else {
        const data = await response.json();
        dispact({ type: "ADD_ACCOUNT", payload: data })
      }
    } catch (err) {
      console.log(err);
    }
  }

  useEffect(() => {
    console.log("perubahan stat");
    if (localStorage.getItem('token')) {
      console.log("token checked");
      aboutMe();
      getHistory();
      getMyItem();
      connect();
    }
  }, [stat]);

  useEffect(() => {
    console.log("effect 1 time");
    getAllBook();
  },[])

  // ---------------------------------------------------------------------------------------------------------

  const message = useSelector(state => state.message);

  useEffect(() => {
    console.log("recive message");
    if (message.action === "BEGIN") {
      console.log("Begin");
      sendMessage(message.payload);
    }
    if (message.action === "LOAD") {
      console.log("LOAD");
      getAllBook();
      getMyItem();
      getHistory();
    }
  }, [message])

  const connect = () => {
    let Sock = new SockJS('http://localhost:8080/ws');
    stompClient = over(Sock);
    stompClient.connect({}, onConnected, onError);
  }

  const onConnected = () => {
    stompClient.subscribe('/topic/public', onMessageReceived);
  }

  const onError = (err) => {
    console.log(err);
  }

  const sendMessage = (id) => {
    var clientMessage = {
      action: "LOAD",
      payload: id
    };
    console.log("sending");
    stompClient.send("/app/message", {}, JSON.stringify(clientMessage));
  }

  const onMessageReceived = (payload) => {
    var payloadData = JSON.parse(payload.body);
    console.log("receive");
    console.log(payloadData);
    dispact({ type: 'SET_MESSAGE', payload: payloadData })
  }

  // ---------------------------------------------------------------------------------------------------------

  return (
    <div className='App'>
      <div>
        <MyNavbar setStat={setStat} />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/login' element={<Login setStat={setStat} />} />
          <Route path='/register' element={<Register />} />
          <Route path='/item'>
            <Route path=':id' element={<PrivateRoute><DetailItem /></PrivateRoute>} />
          </Route>
          <Route path='/myitem' element={<PrivateRoute><MyItem /></PrivateRoute>} />
          <Route path='/profile' element={<PrivateRoute><Profile /></PrivateRoute>} />
          <Route path='/history' element={<PrivateRoute><History /></PrivateRoute>} />
          <Route path='/additem' element={<PrivateRoute><InputForm /></PrivateRoute>} >
            <Route path=':ide' element={<PrivateRoute><InputForm /></PrivateRoute>} />
          </Route>
          <Route path='/*' element={<h1>Page Not Found</h1>} />
        </Routes>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  )
}

export default App