import React from 'react'
import { Container } from 'react-bootstrap'
import HistoryItem from '../component/HistoryItem'
import { useSelector } from 'react-redux';

function History() {

  const histories = useSelector(state => state.mHistory);

  return (
    <Container className='myitem'>
      <h5>My Bid History</h5>
      {
            histories.map(item => (
              <div key={item.id}><HistoryItem item={item} /></div>
              
            ))
          }
    </Container>
  )
}

export default History