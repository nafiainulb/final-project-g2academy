import React, { useState } from 'react'
import { Container, InputGroup } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useDispatch } from 'react-redux';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

function Login({ setStat }) {

    const dispact = useDispatch();
    const [userLogin, setUserLogin] = useState({ username: '', password: '' });

    const location = useLocation();
    const navigate = useNavigate();

    const handleOnChange = (e) => {
        setUserLogin(currUser => {
            return { ...currUser, [e.target.id]: e.target.value }
        })
    }

    const signIn = async (e) => {
        e.preventDefault();
        Swal.fire({
            title: 'Login ...',
            didOpen: () => {
                Swal.showLoading()
            }
        })
        try {
            const response = await fetch(`http://localhost:8080/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(userLogin)
            })
            if (response.ok) {
                const data = await response.json();
                localStorage.setItem('token', data.token);
                Swal.close();
                Swal.fire({
                    icon: 'success',
                    title: 'Login Success',
                    showConfirmButton: false,
                    timer: 1500
                })
                setUserLogin({ username: '', password: '' });
                setStat(1);
                if (location.state) {
                    navigate(`${location.state.from.pathname}`)
                } else {
                    navigate('/');
                }
            } else {
                Swal.close();
                Swal.fire({
                    icon: 'error',
                    title: 'Login Fail',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        } catch (err) {
            console.log(err);
        }
    }


    return (
        <Container fluid="sm">
            <Form>
                <Form.Group className="mb-3">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control id='username' type="email" placeholder="Enter email" onChange={handleOnChange} value={userLogin.username} />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Password</Form.Label>
                    <Form.Control id="password" type="password" placeholder="Password" onChange={handleOnChange} value={userLogin.password} />
                </Form.Group>
                <Form.Group className="mb-3" controlId="notRegister">
                    Not register yet ? <Link to='/register'>Click Here</Link>
                </Form.Group>
                <Button variant="primary" type="submit" onClick={signIn}>
                    Login
                </Button>
            </Form>
        </Container>
    )
}

export default Login