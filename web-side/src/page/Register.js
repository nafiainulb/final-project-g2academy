import React, { useState } from 'react'
import { Container } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

function Register() {

    const [userRegis, setUserRegis] = useState({ username: '', password: '' });
    const navigate = useNavigate();

    const handleOnChange = (e) => {
        setUserRegis(currUser => {
            return { ...currUser, [e.target.id]: e.target.value }
        })
    }

    const registration = async (e) => {
        e.preventDefault();
        Swal.fire({
            title: 'Register Process',
            didOpen: () => {
              Swal.showLoading()
            }
          })
        try {
            const response = await fetch(`http://localhost:8080/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(userRegis)
            })

            if (response.ok) {
                setUserRegis({ username: '', password: '' });
                Swal.close();
                Swal.fire({
                    icon: 'success',
                    title: 'Register Success',
                    showConfirmButton: false,
                    timer: 1500
                })
                navigate('/login');
            } else {
                Swal.close();
                Swal.fire({
                    icon: 'success',
                    title: 'Register Fail',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        } catch (err) {
            console.log(err);
        }
    }

    return (
        <Container >
            <Form>
                <Form.Group className="mb-3">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control id="username" type="email" placeholder="Enter email" onChange={handleOnChange} value={userRegis.username} />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Password</Form.Label>
                    <Form.Control id="password" type="password" placeholder="Password" onChange={handleOnChange} value={userRegis.password} />
                </Form.Group>
                <Button variant="primary" type="submit" onClick={registration}>
                    Register
                </Button>
            </Form>
        </Container>
    )
}

export default Register