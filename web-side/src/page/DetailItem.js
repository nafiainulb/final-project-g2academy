import React, { useEffect, useState } from 'react'
import { Button, Container } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom'
import Swal from 'sweetalert2';

function DetailItem() {

  const { id } = useParams();
  const navigate = useNavigate();
  const account = useSelector(state => state.userAccount);
  const dispact = useDispatch();

  // const bidder = useSelector(state => state.message);

  const [bidder, setBidder] = useState([]);
  const [item, setItem] = useState({ image: '', item_name: '', description: '', current_price: '', post_date: '', end_date: '', user: { username: '' } });
  const [amount, setAmount] = useState(0);

  const getBidder = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/bidder/${id}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
      if (response.ok) {
        const data = await response.json();
        console.log(data);
        dispact({ type: "SET_BIDDER", payload: data });
        setBidder(data);
      } else {
        throw Error('something wrong');
      }
    } catch (err) {
      console.log(err);
    }
  }

  const getItem = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/product/${id}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
      if (response.status !== 200) {
        throw Error('something wrong')
      } else {
        const data = await response.json();
        setItem(data);
        console.log("done");
      }
    } catch (err) {
      console.log(err);
    }
  }

  const bidding = async () => {
    Swal.fire({
      title: 'Uploading Process',
      didOpen: () => {
        Swal.showLoading()
      }
    })
    try {
      const response = await fetch(`http://localhost:8080/api/bid/${id}`, {
        method: 'POST',
        body: JSON.stringify({ bid: amount }),
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      });
      if (response.status !== 200) {
        Swal.close();
        throw Error('something wrong')
      } else {
        const data = await response.text();
        console.log(data);
        dispact({ type: "SET_MESSAGE", payload: { action: "BEGIN", payload: id } });
        Swal.close();
        Swal.fire({
          icon: 'info',
          title: data,
          showConfirmButton: false,
          timer: 1500
        })
        console.log("done");
      }
    } catch (err) {
      Swal.close();
      console.log(err);
    }
  }

  // ---------------------------------------------------------------------------------------------------------

  const message = useSelector(state => state.message);

  useEffect(() => {
    console.log("Renew bidder");
    if (message.action === "LOAD") {
      console.log("Renew bidder v1");
      if (message.payload === id) {
        console.log("Renew bidder v2");
        getBidder();
      }
    }
  }, [message])

  // ---------------------------------------------------------------------------------------------------------

  const editing = () => {
    navigate(`/additem/${id}`)
  }

  useEffect(() => {
    if (localStorage.getItem('token')) {
      getItem();
      getBidder();
    }
  }, []);

  return (
    <Container>
      {account.username === item.user.username && <Button onClick={editing}>Edit</Button>}
      <div className='listItem1'>
        <div className='itemBox1'>
          <div>
            <div className='imageBox'>
              <img src={item.image} className='imageItem' alt='photoku' />
            </div>
            <div className='desc'>
              <div className='textitem'><b>{item.item_name}</b></div>
              <div className='textitem'><b>Current Bid: </b>
                {item.current_price.toLocaleString('en-EN', {
                  style: 'currency',
                  currency: 'USD'
                })}
              </div>
              <div className='textitem'><b>Owner:</b> {item.user.username}</div>
              <div className='textitem'><b>Publish: </b>
                {new Date(item.post_date).toLocaleString('en-us', {
                  year: "numeric", month: "short", day: "numeric"
                })}
              </div>
              <div className='textitem'><b>End date: </b>
                {new Date(item.end_date).toLocaleString('en-us', {
                  year: "numeric", month: "short", day: "numeric"
                })}
              </div>
              {account.username !== item.user.username &&
                <div className='bid'>
                  <input onChange={e => setAmount(e.target.value)}></input>
                  <button onClick={bidding}>Bid</button>
                </div>
              }
            </div>
          </div>
          <div className='bidderList'>
            <div className='bidderItem'><b>Bidder List</b></div>

            {
              bidder.map(item => (
                <div key={item.id}>
                  <div className='bidderItem'>
                    <div className='textitem'>{item.user.username}</div>
                    <div className='textitem'>
                      {item.bid.toLocaleString('en-EN', {
                        style: 'currency',
                        currency: 'USD'
                      })}
                    </div>
                  </div>
                </div>
              ))
            }
          </div>
          <div className='desc'>
            <h6>Description:</h6>
            <p>{item.description}</p>
          </div>
        </div>
      </div>
    </Container >
  )
}

export default DetailItem