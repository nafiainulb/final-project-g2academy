import React from 'react'
import { Button, Container } from 'react-bootstrap'
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import AuctionBoxItem from '../component/AuctionBoxItem'

function MyItem() {

  const navigate = useNavigate();
  const myitem = useSelector(state => state.mItem);

  const additem = () => {
    navigate('/additem');
  }

  return (
    <Container className='myitem'>
      <Button onClick={additem}>Add Item</Button>
      <div className='listItem'>
        {
            myitem.map(item => (
              <div key={item.id}><AuctionBoxItem item={item} /></div>
              
            ))
          }
      </div>
    </Container>
  )
}

export default MyItem