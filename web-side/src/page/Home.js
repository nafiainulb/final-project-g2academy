import Button from 'react-bootstrap/Button';
import React from 'react'
import { Container } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux';

import AuctionBoxItem from '../component/AuctionBoxItem';


function Home() {

  const itemList = useSelector(state => state.itemList);

  


  return (
    <div>
      <Container>
        <h5>Auction List</h5>
        {/* <Button variant="primary" type="submit" onClick={connect}>
          Connect
        </Button> */}
        <div className='listItem'>
          {
            itemList.map(item => (
              <div key={item.id}><AuctionBoxItem item={item} /></div>
            ))
          }
        </div>
      </Container>
    </div>
  )
}

export default Home