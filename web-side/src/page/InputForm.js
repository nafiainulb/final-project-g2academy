import React, { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useNavigate, useParams } from 'react-router-dom';

import { deleteObject, getDownloadURL, ref, uploadBytes } from 'firebase/storage';
import { storage } from '../firebase';
import Swal from 'sweetalert2';
import { useDispatch } from 'react-redux';

function InputForm() {

  const dispact = useDispatch();
  const navigate = useNavigate();
  const { ide } = useParams();

  const [uploadImage, setUploadImage] = useState(null);
  const [urlImage, setUrlImage] = useState(null);

  const [newItem, setNewItem] = useState({ item_name: '', current_price: '', description: '', image: null, end_date: '' });

  const addBook = async (downloadUrl, metode, kodek) => {
    try {
      const temp = { ...newItem, 'image': downloadUrl };
      // console.log("my item");
      // console.log(temp);
      const response = await fetch(`http://localhost:8080/api/product/${kodek}`, {
        method: `${metode}`,
        body: JSON.stringify(temp),
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      });
      if (response.ok) {
        let data = await response.json();
        if (metode === 'POST') {
          dispact({ type: "ADD_ITEM", payload: data })
        } else {
          dispact({ type: "UPDATE_ITEM", payload: data })
        }
        dispact({ type: "SET_MESSAGE", payload: {action: "LOAD"} });
        Swal.close();
        Swal.fire({
          icon: 'success',
          title: 'Book has been saved',
          showConfirmButton: false,
          timer: 1500
        }).then((result) => {
          navigate('/myitem');
        })
      } else {
        throw Error('something wrong')
      }
    } catch (err) {
      console.log(err);
    }
  }

  useEffect(() => {
    console.log("runer");
    console.log(ide);
    if (parseInt(ide) > 0) {
      console.log("burner");
      const getBook = async () => {
        try {
          const response = await fetch(`http://localhost:8080/api/product/${ide}`, {
            method: 'GET',
            headers: {
              'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
          });
          if (response.ok) {
            const data = await response.json();
            console.log(data);
            setNewItem(data);
            setUrlImage(data.image);
          } else {

            throw Error('something wrong');
          }
        } catch (err) {
          console.log(err);
        }
      }
      getBook();
    }
  }, [ide])

  const deleteBook = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/product/${ide}`, {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
      if (response.ok) {
        dispact({ type: "DELETE_ITEM", payload: Number(ide) });
        deleted();
        Swal.fire({
          icon: 'success',
          title: 'Item Deleted',
          showConfirmButton: false,
          timer: 1500
        }).then((result) => {
          navigate('/myitem');
        })
      } else {
        throw Error('something wrong');
      }
    } catch (err) {
      console.log(err);
    }
  }

  const onChangeBook = (e) => {
    setNewItem(currState => {
      return { ...currState, [e.target.id]: e.target.value }
    })
  }

  const reset = () => {
    setNewItem({ item_name: '', current_price: '', description: '', image: '', end_date: '' });
  }

  const save = (e) => {
    e.preventDefault();
    upload();
    reset();
  }

  const deleted = () => {

    if (newItem.image !== null) {
      // Create a reference to the file to delete
      console.log("run delete");
      const desertRef = ref(storage, urlImage);

      // Delete the file
      deleteObject(desertRef).then(() => {
        console.log("delete image success");

        // File deleted successfully
      }).catch((error) => {
        console.log(error);
        // Uh-oh, an error occurred!
      });
    }

  }

  const upload = () => {

    Swal.fire({
      title: 'Uploading Process',
      didOpen: () => {
        Swal.showLoading()
      }
    })

    let downloadUrl = newItem.image;
    if (uploadImage !== null) {
      console.log("delete begin");
      getUrl();
      deleted();
    } else if (urlImage) {
      console.log("put begin");
      addBook(downloadUrl, 'PUT', ide);
    } else {
      console.log("post begin");
      addBook(downloadUrl, 'POST', '');
    }


  }

  const getUrl = () => {
    // untuk storage
    let fileName = uploadImage.name;
    const storageRef = ref(storage, `images/${fileName}`);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, uploadImage)
      .then(snapshot => {
        getDownloadURL(storageRef)
          .then(downloadUrl => {
            // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
            setUrlImage(downloadUrl);
            console.log("run upload");
            if (urlImage) {
              console.log("get put");
              addBook(downloadUrl, 'PUT', ide);
            } else {
              console.log("get post");
              addBook(downloadUrl, 'POST', '');
            }

          }).catch((error) => {
            console.log(error);
            // Uh-oh, an error occurred!
          })

      })
      .catch(err => {
        console.log(err);
      })
  }

  return (
    <Container>
      <Form>
        <Form.Group className="mb-3">
          <Form.Label>Item Name</Form.Label>
          <Form.Control id='item_name' type="text" placeholder="Item Name" value={newItem.item_name} onChange={onChangeBook} />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Initial Price</Form.Label>
          <Form.Control id='current_price' type="number" placeholder="Initial Price" value={newItem.current_price} onChange={onChangeBook} />
        </Form.Group>
        <Form.Group className="mb-3" >
          <Form.Label>Description</Form.Label>
          <Form.Control id='description' as="textarea" rows={4} value={newItem.description} onChange={onChangeBook} />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Image</Form.Label>
          <Form.Control type="file" onChange={e => setUploadImage(e.target.files[0])} />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>End Bidding Date</Form.Label>
          <Form.Control id='end_date' type="date" value={newItem.end_date.split('T')[0]} onChange={onChangeBook} />
        </Form.Group>

        <Button variant="primary" type="submit" onClick={save}>
          Submit
        </Button>
        {ide && <Button variant="primary" type="submit" onClick={deleteBook}>
          Delete
        </Button>}

      </Form>
    </Container>
  )
}

export default InputForm