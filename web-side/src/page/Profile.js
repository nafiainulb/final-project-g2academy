import React, { useEffect, useState } from 'react'
import { Container } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

function Profile() {

  const [my, setMy] = useState({ short_name: '', birthDate: '1111-11-11', gender: '', address: '' });

  const profile = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/profile`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
      if (response.status !== 200) {
        throw Error('something wrong')
      } else {
        const data = await response.json();
        console.log(data);
        setMy(data);
      }
    } catch (err) {
      console.log(err);
    }
  }

  const saveprofile = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/profile`, {
        method: 'PUT',
        body: JSON.stringify(my),
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      });
      if (response.status !== 200) {
        throw Error('something wrong')
      } else {
        console.log("Done");
      }
    } catch (err) {
      console.log(err);
    }
  }

  useEffect(() => {
    profile();
  }, [])

  const sendData = (e) => {
    e.preventDefault();
    saveprofile();
  }

  return (
    <Container>
      <Form>
        <Form.Group className="mb-3">
          <Form.Label>Name</Form.Label>
          <Form.Control type="text" placeholder="Name" value={my.short_name} onChange={e => setMy({...my, short_name: e.target.value})}/>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Adrress</Form.Label>
          <Form.Control type="text" placeholder="Adrress" value={my.address} onChange={e => setMy({...my, address: e.target.value})}/>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Gender</Form.Label>
          <Form.Select aria-label="Default select example" value={my.gender} onChange={e => setMy({...my, gender: e.target.value})}>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Birth Date</Form.Label>
          <Form.Control id='birthDate' type="date" value={my.birthDate.split('T')[0]} onChange={e => setMy({...my, birthDate: e.target.value})}/>
        </Form.Group>
        <Button variant="primary" type="submit" onClick={sendData}>
          Submit
        </Button>
      </Form>
    </Container>
  )
}

export default Profile