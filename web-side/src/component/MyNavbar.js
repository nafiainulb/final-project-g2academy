import { Button, Form } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { useDispatch } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';


function MyNavbar({ setStat }) {

  const navigate = useNavigate();
  const dispact = useDispatch();

  const login = () => {
    navigate('/login');
  }

  const register = () => {
    navigate('/register');
  }

  const signOut = () => {
    localStorage.removeItem('token');
    setStat(0);
    dispact({ type: "DELETE_ACCOUNT" });
    dispact({ type: "DELETE_MYITEM" });
    dispact({ type: "DELETE_HISTORY" });
    navigate('/login');
  }

  return (
    <Navbar bg='dark' expand="lg" className='mb-3 navitem'>
      <Container>

        <Form className="d-flex logo">
          <img
            src='https://firebasestorage.googleapis.com/v0/b/book-store-datastore.appspot.com/o/auction-today-shadow-white.png?alt=media&token=484f96c1-3f12-461f-986a-31d96396b364'
            width="90"
            height="30" className="d-inline-block align-top"
            alt="Auction Today"
          />
        </Form>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Link className="nav-link active text-light" to="/">Home</Link>
            {localStorage.getItem('token') && <Link className="nav-link active text-light" to="/myitem">My Item</Link>}
            {localStorage.getItem('token') && <Link className="nav-link active text-light" to="/history">History</Link>}
            {localStorage.getItem('token') && <Link className="nav-link active text-light" to="/profile">Profile</Link>}

          </Nav>
        </Navbar.Collapse>
        <Form className="d-flex">
          {
            !localStorage.getItem('token') ?
              <div>
                <Button variant="light" size="sm" onClick={login}>Login</Button>
                <Button variant="light" size="sm" onClick={register}>Register</Button>
              </div>
              :
              <Button variant="light" size="sm" onClick={signOut}>Logout</Button>
          }

        </Form>
      </Container>
    </Navbar>
  )
}

export default MyNavbar