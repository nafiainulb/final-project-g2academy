import React from 'react'

function HistoryItem({ item }) {
    return (
        <div className='historyitem'>
            <div className='imageBox1'>
                <img src={item.image} className='imageItem' alt='photoku' />
            </div>
            <div className='textitem'><b>{item.item_name}</b></div>
            <div>
                Bid {item.bid.toLocaleString('en-EN', {
                    style: 'currency',
                    currency: 'USD'
                })}
            </div>
        </div>
    )
}

export default HistoryItem