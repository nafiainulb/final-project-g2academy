import React from 'react'
import { useNavigate } from 'react-router-dom'

function AuctionBoxItem({ item }) {

  const navigate = useNavigate();

  const gotoDetail = () => {
    navigate(`/item/${item.id}`)
  }

  return (
    <div className='itemBox'>
      <div className='imageBox'>
        <img src={item.image} className='imageItem' alt='photoku' />
      </div>
      <div className='textBox'>
        <div>
          <div className='textitem'><b>{item.item_name}</b></div>
          <div className='textitem'><b>Price: </b>
            {item.current_price.toLocaleString('en-EN', {
                  style: 'currency',
                  currency: 'USD'
                })}
          </div>
          <div className='textitem'><b>Owner:</b> {item.user.username}</div>
          <div className='textitem'><b>End date: </b>
            {new Date(item.end_date).toLocaleString('en-us', {
              year: "numeric", month: "short", day: "numeric"
            })}
          </div>
        </div>
        <button className="btn btn-primary" type="button" onClick={gotoDetail} >Detail</button>
      </div>

    </div>
  )
}

export default AuctionBoxItem